#include <iostream>
#include "GregorianCalendar.h"
//#include "DateInterval.h"

int main()
{
	GregorianCalendar gc;
	std::cout << "Current: " << gc.toString() << std::endl;
	std::cout << gc.getYear() << " is leap year: " << std::boolalpha << gc.isLeapYear() << std::endl;

	GregorianCalendar gc2(25, (Month)5, 2050, 25, 32, 23);
	std::cout << "Full constructor: " << gc2.toString() << std::endl;

	GregorianCalendar gc3(2020, (Month)5, 25);
	std::cout << "Date constructor: " << gc3.toString() << std::endl;

	GregorianCalendar gc4(21, 12, 47);
	std::cout << "Time constructor: " << gc4.toString() << std::endl;

	std::cout << std::endl;
	GregorianCalendar temp;
	GregorianCalendar gc5 = temp.addSeconds(94200);
	std::cout << "Current: " << temp.toString() << std::endl;
	std::cout << "Add 94200 seconds from current: " << gc5.toString() << std::endl;
	gc5 = gc5.addDays(-1205);
	std::cout << "Remove 1205 days from last: " << gc5.toString() << std::endl;

	//DateInterval d;
	//std::cout << d.toString();
}
