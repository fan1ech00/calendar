#pragma once
#include <iostream>
#include "DateInterval.h"

enum class Month {
	Jan = 1,
	Feb,
	Mar,
	Apr,
	May,
	June,
	July,
	Aug,
	Sept,
	Oct,
	Nov,
	Dec
};

class GregorianCalendar
{
	unsigned int day_;
	Month month_;
	unsigned int year_;
	unsigned int seconds_;
	unsigned int minutes_;
	unsigned int hours_;

	void normalize();

	unsigned int getTimeInSeconds() const;
	void setTimeFromSeconds(unsigned int seconds);

	unsigned int getInDays() const;
	void daysToDate(unsigned int days);
	unsigned int getDaysFromYears() const;
	unsigned int getDaysFromMonth() const;

public:
	// ������������ �������� ����� ������
	//DateInterval getInterval(const GregorianCalendar& a) const;
	//DateInterval getInterval(const GregorianCalendar& another) const;
	//// ������������ ����� ���� ������������ ��������.
	//GregorianCalendar addInterval(const DateInterval& a) const;

	bool isLeapYear();
	static bool isLeapYear(unsigned int year);

	std::string toString() const; 
	unsigned int getDay() const;
	unsigned int getMonth() const;
	unsigned int getYear() const;
	unsigned int getSeconds() const;
	unsigned int getMinutes() const;
	unsigned int getHours() const;

	//��������� ������� addXXX ����� ���� ��������������, �� ��� ���� ���� �� ����� ���� ������ 
	//	0:00 : 00 1 Jan 1 ��� ������ 9999 - 12 - 31 23 : 59 : 59
	GregorianCalendar addYears(int years);
	GregorianCalendar addMonths(int months);
	GregorianCalendar addDays(int days);
	GregorianCalendar addHours(int hours);
	GregorianCalendar addMinutes(int minutes);
	GregorianCalendar addSeconds(int seconds);

	GregorianCalendar operator= (const GregorianCalendar& gc);
	GregorianCalendar();
	GregorianCalendar(unsigned int year, Month month, unsigned int day); 
	GregorianCalendar(unsigned int hours, unsigned int minutes, unsigned int seconds);
	GregorianCalendar(unsigned int day, Month month, unsigned int year,
		unsigned int seconds, unsigned int minutes, unsigned int hours);
};

