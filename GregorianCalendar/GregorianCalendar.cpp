#include "GregorianCalendar.h"
#include <time.h>
#include <string>

GregorianCalendar GregorianCalendar::addYears(int years)
{
	GregorianCalendar t = *this;
	if (!years > t.year_) {
		t.year_ = 1;
		return t;
	}

	t.year_ += years;
	t.normalize();
	return t;
}

GregorianCalendar GregorianCalendar::addMonths(int months)
{
	GregorianCalendar t = *this;
	int i = (int)t.month_ + months;
	if (i > 0) {
		while (i > 12) {
			t.year_++;
			i -= 12;
		}
	}
	else {
		while (i <= 0) {
			t.year_--;
			i += 12;
		}
	}

	t.month_ = (Month)i;
	t.normalize();
	return t;
}

GregorianCalendar GregorianCalendar::addDays(int days)
{
	GregorianCalendar t = *this;
	t.day_ += days;
	t.normalize();
	return t;
}

GregorianCalendar GregorianCalendar::addHours(int hours)
{
	GregorianCalendar t = *this;
	if (!hours > t.getTimeInSeconds() / 60 / 60) {
		t.seconds_ = 0;
		t.minutes_ = 0;
		t.hours_ = 0;
		return t;
	}
	t.hours_ += hours;
	t.normalize();
	return t;
}

GregorianCalendar GregorianCalendar::addMinutes(int minutes)
{
	GregorianCalendar t = *this;
	if (!minutes > t.getTimeInSeconds() / 60) {
		t.seconds_ = 0;
		t.minutes_ = 0;
		t.hours_ = 0;
		return t;
	}
	t.minutes_ += minutes;
	t.normalize();
	return t;
}

GregorianCalendar GregorianCalendar::addSeconds(int seconds)
{
	GregorianCalendar t = *this;
	if (!seconds > t.getTimeInSeconds()) {
		t.seconds_ = 0;
		t.minutes_ = 0;
		t.hours_ = 0;
		return t;
	}
	t.seconds_ += seconds;
	t.normalize();
	return t;
}

GregorianCalendar GregorianCalendar::operator=(const GregorianCalendar& gc)
{
	this->day_ = gc.day_;
	this->month_ = gc.month_;
	this->year_ = gc.year_;
	this->seconds_ = gc.seconds_;
	this->minutes_ = gc.minutes_;
	this->hours_ = gc.hours_;
	return *this;
}

GregorianCalendar::GregorianCalendar()
{
	struct tm date;
	time_t now = time(0);
	localtime_s(&date, &now);

	day_ = date.tm_mday;
	month_ = (Month)(date.tm_mon + 1);
	year_ = date.tm_year + 1900;
	seconds_ = date.tm_sec;
	minutes_ = date.tm_min;
	hours_ = date.tm_hour;
}

GregorianCalendar::GregorianCalendar(unsigned int day, Month month, unsigned int year, 
	unsigned int seconds, unsigned int minutes, unsigned int hours)
{
	year_ = year;
	month_ = month;
	day_ = day;

	seconds_ = seconds;
	minutes_ = minutes;
	hours_ = hours;
	
	normalize();
}

GregorianCalendar::GregorianCalendar(unsigned int year, Month month, unsigned int day)
{
	year_ = year;
	month_ = month;
	day_ = day;

	seconds_ = 0;
	minutes_ = 0;
	hours_ = 0;

	normalize();
}

GregorianCalendar::GregorianCalendar(unsigned int hours, unsigned int minutes, unsigned int seconds) : GregorianCalendar()
{
	hours_ = hours;
	minutes_ = minutes;
	seconds_ = seconds;
}

void GregorianCalendar::normalize()
{
	setTimeFromSeconds(seconds_ + (minutes_ * 60) + (hours_ * 60 * 60));
	daysToDate(getInDays());
	if (year_ > 9999)
		year_ = 9999;
}

unsigned int GregorianCalendar::getTimeInSeconds() const
{
	return seconds_ + (minutes_ * 60) + (hours_ * 60 * 60);
}

void GregorianCalendar::setTimeFromSeconds(unsigned int seconds)
{
	if (seconds == 0)
		return;
	auto daySec = 86400;
	while (seconds >= daySec) {
		day_++;
		seconds -= daySec;
	}

	hours_ = seconds / (60 * 60);
	seconds -= hours_ * 60 * 60;

	minutes_ = seconds / 60;
	seconds -= minutes_ * 60;

	seconds_ = seconds;
}

unsigned int GregorianCalendar::getDaysFromYears() const
{
	unsigned int days = 0;
	
	for (unsigned int i = 1; i < year_; i++) {
		if (GregorianCalendar::isLeapYear(i))
			days += 366;
		else
			days += 365;
	}
	return days;
}

unsigned int GregorianCalendar::getDaysFromMonth() const
{
	unsigned int days = 0;
	switch ((Month)((int)month_ - 1)) {
	//case Month::Dec:  days += 31;
	case Month::Nov:  days += 30;
	case Month::Oct:  days += 31;
	case Month::Sept: days += 30;
	case Month::Aug:  days += 31;
	case Month::July: days += 31;
	case Month::June: days += 30;
	case Month::May:  days += 31;
	case Month::Apr:  days += 30;
	case Month::Mar:  days += 31;
	case Month::Feb:  days += this->isLeapYear(year_) ? 29 : 28;
	case Month::Jan:  days += 31;
	default:          break;
	}
	return days;
}

unsigned int GregorianCalendar::getInDays() const
{
	unsigned int days = 0;
	days += getDaysFromYears();
	days += getDaysFromMonth();
	days += day_ - 1; //  - nowaday
	return days;
}

void GregorianCalendar::daysToDate(unsigned int days)
{
	year_ = 1;
	month_ = (Month)1;
	day_ = 1;

	while (isLeapYear(year_) ? days / 366 >= 1 : days / 365 >= 1) {
	//while (days / 366 >= 1) {
		if (isLeapYear(year_))
			days -= 366;
		else
			days -= 365;
		year_ += 1;
	}

	for (int i = 12; i > 0; i--) { 
		month_ = (Month)i;
		unsigned int monthDays = this->getDaysFromMonth();
		if (days >= monthDays) {
			days -= monthDays;
			break;
		}
	}

	day_ += days;
}

bool GregorianCalendar::isLeapYear()
{
	return GregorianCalendar::isLeapYear(year_);
}

bool GregorianCalendar::isLeapYear(unsigned int year)
{
	if (year % 4 == 0) {
		if (year % 400 == 0)
			return true;
		else if (year % 100 == 0)
			return false;
		else
			return true;
	}
	else
		return false;
}

std::string GregorianCalendar::toString() const
{
	std::string str = "";
	std::string arrMonth[12] {
		"Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"
	};

	str += std::to_string(year_);
	str += " - ";
	str += arrMonth[(int)month_ - 1];
	str += " - ";
	str += std::to_string(day_);
	str += " ";
	str += std::to_string(hours_);
	str += "::";
	str += std::to_string(minutes_);
	str += "::";
	str += std::to_string(seconds_);

	return str;
}

unsigned int GregorianCalendar::getDay() const
{
	return day_;
}

unsigned int GregorianCalendar::getMonth() const
{
	return (int)month_;
}

unsigned int GregorianCalendar::getYear() const
{
	return year_;
}

unsigned int GregorianCalendar::getSeconds() const
{
	return seconds_;
}

unsigned int GregorianCalendar::getMinutes() const
{
	return minutes_;
}

unsigned int GregorianCalendar::getHours() const
{
	return hours_;
}
