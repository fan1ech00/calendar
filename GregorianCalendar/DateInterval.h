#pragma once
#include <iostream>

//������������� ����������� ����� DateInterval, ����������� ���������� ���������� ��������� 
//����� ������ � ������ �����, �������, ����, �����, �������, ��������(�������� ����� ���� ��������������).
//DateInterval ���������� ������ ������� ������� � ����������, ������������� �����, ���������� ������������.
//
//����� Date ��������� ������� DateInterval getInterval(const Date& another) const ������������ ��������
//����� ������ � ������� Date addInterval(const DateInterval&) const ������������ ����� ���� ������������ 
//��������.
//
//����������� ����� std::string formatDate(std::string format) ����������� ���������� ������ ������� ��� 
//������������� ���� � ���� ������.��������� ��������� � ������ ������� �������� :
//YYYY
//MM(����� � �������� �������)
//MMM(����� � ������� Jan, Feb... Dec)
//DD
//hh
//mm
//ss
//��������� ������� ������ ���� �������� � �������� ������ ��� ���������.� ������ ���� ������ ������� 
//�����������, ���������� ������ �Invalid date format�
//
//�������� �������� ��������� ��������������� ������ � ����������� � ��������������� ����.

class DateInterval
{
	int day_;
	int month_;
	int year_;
	int seconds_;
	int minutes_;
	int hours_;

	void normalize();

	unsigned int getTimeInSeconds() const;
	void setTimeFromSeconds(unsigned int seconds);

	unsigned int getInDays() const;
	void daysToDate(unsigned int days);
	unsigned int getDaysFromYears() const;
	unsigned int getDaysFromMonth() const;
public:
	//����������� ���������� ������ ������� ��� ������������� ���� � ���� ������.
	std::string formatDate(std::string format);

	DateInterval();
};

